import sys
how_many_gram=int(sys.argv[1])
arpa_file=sys.argv[2]
arpa_rows=[e.strip() for e in open(arpa_file)]
vocabulary=[r[:-1].strip().split('\x00') for r in open(sys.argv[3])][0]

assert len(vocabulary)==int(arpa_rows[1].split("=")[1]), (len(vocabulary), int(arpa_rows[1].split("=")[1]))

#The sum of prob (not backoff prob) of all unigram (except <unk>) is equal to 1
number_of_unigram = int(arpa_rows[1].split("=")[1])
sum_of_all_unigram_prob_except_start=sum(10**float(e.split()[0]) for e in arpa_rows[3+how_many_gram+1: 3+how_many_gram+number_of_unigram])
assert abs(1-sum_of_all_unigram_prob_except_start) < 1e-5, (sum_of_all_unigram_prob_except_start, number_of_unigram)
def read_arpa_lm_file(path):
        p={} #probability
        b={} #backoff
        with open(path) as f:
            for row in f:
                row=row.strip().split("\t")
                if len(row)>=2:
                    p[row[1]]=float(row[0])
                    if len(row)==3:
                        b[row[1]]=float(row[2])
        return (p,b)
(p,b) = read_arpa_lm_file(arpa_file)
def approx_equal(a,b):
    return abs(a-b) < 1e-5

import sys, readline, os
sys.path.append(os.path.expanduser( \
            r"~/.pip/kenlm/lib/python2.7/site-packages/"))
import kenlm

if how_many_gram==1:
    #The LM file can be validated simply by figuring out some fundamental qaunt.
    model=kenlm.LanguageModel("arpa1.special")
    s=model.score
    assert approx_equal(s("<s>"), p["<s>"]+p[r"</s>"])
    #This shows that the unigram model has </s> at the termination.
    #The strings are assigned probability as
    #p(</s>) + p(</s>) * sum(p(h), p(he), p(hell), p(unk)) + ... )

if how_many_gram >= 2:
    model=kenlm.LanguageModel(arpa_file)
    
    assert approx_equal(model.score("kenlm"), p[r"kenlm </s>"] + p[r"<s> kenlm"])
    assert approx_equal(model.score("hugabuga"), p["<unk>"]+b["<s>"]+p["</s>"])
    # Note that it does not treat <s> as an OOV
    # it also won't treat ()</s> <unk>) as OOV
    # This is a bug in the kenlm_python binding binary.
    assert approx_equal(model.score("<s>"), p[r"<s> </s>"]+p["<s>"]+b["<s>"])
    
    
